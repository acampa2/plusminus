import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;


//Given an array of integers, calculate the fractions of its elements that are positive, negative, and are zeros.
// Print the decimal value of each fraction on a new line...

public class Main {

    // Complete the plusMinus function below.
    static void plusMinus(int[] arr) {
        float pos=0, neg=0, zero=0;
        float arraySize = arr.length;
        float posResult =0, negResult=0, zeroResult=0;
        for(int i=0; i<arr.length;i++){
            if(arr[i]>0)
                pos++;
            else if (arr[i]<0)
                neg++;
            else if(arr[i] ==0)
                zero++;
        }
        posResult = pos/arraySize;
        negResult = neg/arraySize;
        zeroResult = zero/arraySize;

        System.out.println(posResult);
        System.out.println(negResult);
        System.out.println(zeroResult);
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = 6;
        int[] arr = {-4,3,-9,0,4,1};

        plusMinus(arr);

    }
}
